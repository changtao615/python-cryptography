%global pypi_name cryptography
Name:           python-%{pypi_name}
Version:        42.0.7
Release:        2
Summary:        PyCA's cryptography library
License:        (Apache-2.0 OR BSD-3-Clause) AND PSF-2.0
URL:            https://cryptography.io/en/latest/
Source0:        %{pypi_source %{pypi_name}}
# For Rust offline compile
# Decompress the source code of cryptography, then enter ./src/rust directory,
# execute "cargo vendor" to obtain "vendor" directory (Internet connection required),
# finally, tar -czvf cargo-vendor-cache.tar.gz vendor
# Note: Cargo needs to be consistent with the cargo version in the compile environment.
Source1:        cargo-vendor-cache.tar.gz

Patch6002:      backport-provide-openssl-apis-related-to-SM-for-python.patch

BuildRequires:  openssl-devel cargo
BuildRequires:  gcc
BuildRequires:  rust-packaging rust

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-pytest >= 3.2.1
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-pretend
BuildRequires:  python%{python3_pkgversion}-iso8601
BuildRequires:  python%{python3_pkgversion}-cryptography-vectors = %{version}
BuildRequires:  python%{python3_pkgversion}-hypothesis >= 1.11.4
BuildRequires:  python%{python3_pkgversion}-pytz
BuildRequires:  python%{python3_pkgversion}-cffi >= 1.12
BuildRequires:  python%{python3_pkgversion}-setuptools-rust >= 1.7.0
BuildRequires:  python%{python3_pkgversion}-wheel
BuildRequires:  python3-pip
BuildRequires:  python3-pytest-subtests
BuildRequires:  python3-pytest-benchmark
BuildRequires:  python3-certifi
BuildRequires:  python3-bcrypt

%description
cryptography is a package designed to expose cryptographic primitives and
recipes to Python developers.


%package     -n python%{python3_pkgversion}-%{pypi_name}
Summary:        PyCA's cryptography library

Requires:       openssl-libs
Requires:       python%{python3_pkgversion}-cffi >= 1.12

%{?python_provide:%python_provide python%{python3_pkgversion}-%{pypi_name}}

%description -n python%{python3_pkgversion}-%{pypi_name}
cryptography is a package designed to expose cryptographic primitives and
recipes to Python developers.

%package_help

%prep
%autosetup -n %{pypi_name}-%{version} -p1
tar xzvf %{SOURCE1} -C ./src/rust/
mkdir .cargo
cat >> .cargo/config.toml << EOF
[source.crates-io]
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "src/rust/vendor"

[profile.release]
debug = true # https://doc.rust-lang.org/rustc/codegen-options/index.html#debuginfo
EOF


%build
%pyproject_build

%install
%pyproject_install

%check
PYTHONPATH=%{buildroot}%{python3_sitearch} %{__python3} -m pytest --ignore vendor

%files -n python%{python3_pkgversion}-%{pypi_name}
%defattr(-,root,root)
#%doc AUTHORS.rst
%license LICENSE LICENSE.APACHE LICENSE.BSD
%{python3_sitearch}/%{pypi_name}
%{python3_sitearch}/%{pypi_name}-%{version}.dist-info

%files help
%defattr(-,root,root)
%doc README.rst docs

%changelog
* Fri Jul 26 2024 shixuantong <shixuantong1@huawei.com> - 42.0.7-2
- enable check

* Fri Jul 19 2024 liweigang <liweigang@uniontech.com> - 42.0.7-1
- update to version 42.0.7
- license compliance rectification

* Thu Apr 18 2024 shixuantong <shixuantong1@huawei.com> - 42.0.2-3
- set debug is true for build debug package
- add rust to BuildRequires

* Tue Feb 27 2024 shixuantong <shixuantong1@huawei.com> - 42.0.2-2
- fix CVE-2024-26130

* Thu Feb 01 2024 shixuantong <shixuantong1@huawei.com> - 42.0.2-1
- upgrade version to 42.0.2

* Sat Dec 23 2023 shixuanttong <shixuantong1@huawei.com> - 40.0.2-5
- update author info for Patch6002

* Sat Dec 2 2023 liningjie <liningjie@xfusion.com> - 40.0.2-4
- raise an exception instead of returning an empty list for pkcs7 cert loading

* Wed Nov 29 2023 liningjie <liningjie@xfusion.com> - 40.0.2-3
- Fixed crash when loading a PKCS#7 bundle with no certificates

* Tue Jul 25 2023 shixuantong <shixuantong1@huawei.com> - 40.0.2-2
- fix CVE-2023-38325

* Fri May 19 2023 Dongxing Wang <dxwangk@isoftstone.com> - 40.0.2-1
- Upgrade package to 40.0.2

* Tue Feb 14 2023 zhuofeng<zhuofeng2@huawei.com> - 39.0.0-2
- Type:CVE
- CVE:CVE-2023-23931
- SUG:NA
- DESC:fix CVE-2023-23931

* Fri Feb 3 2023 huangduirong <huangduirong@huawei.com> - 39.0.0-1
- Upgrade package to 39.0.0

* Tue Jul 19 2022 huangtianhua <huangtianhua@huawei.com> - 36.0.1-1
- Upgrade package to 36.0.1

* Fri Jul 01 2022 tianwei<tianwei12@h-partners.com> -3.3.1-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: provide openssl apis related to SM for python

* Thu Jun 30 2022 tianwei<tianwei12@h-partners.com> -3.3.1-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: add SM4 symmetric block cipher

* Tue Feb 23 2021 shixuantong <shixuantong@huawei.com> - 3.3.1-2
- fix CVE-2020-36242

* Mon Feb 1 2021 liudabo <liudabo1@huawei.com> - 3.3.1-1
- upgrade version to 3.3.1

* Tue Aug 11 2020 tianwei<tianwei12@huawei.com> -3.0-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete python2  

* Thu Jul 23 2020 dingyue<dingyue5@huawei.com> -3.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:NA

* Thu Apr 16 2020 chengquan<chengquan3@huawei.com> -2.9-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade software to v2.9

* Thu Feb 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.6.1-1
- Update to 2.6.1

* Tue Oct 22 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.3-5
- Package rebuild.

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.3-4
- Type: enhancement
- ID:   NA
- SUG:  NA
- DESC: fix build failed.

* Sat Sep 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.3-3
- Package init.
